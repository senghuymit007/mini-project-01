package com.senghuy.miniproject;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {

    String[] play_role = {"GK", "CB", "CMF", "DMF", "AMF", "LWF", "RWF", "CF"};
    Spinner spinner;

    Button btn_date, btn_save;
    TextView text_view_date;

    AppCompatEditText et_name_title;
    Dialog dialog_message;
    private int mYear, mMonth, mDay;

    RadioGroup radioGroup;
    RadioButton selectedRadioButton;
    String selectedRbText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        btn_date = findViewById(R.id.btn_date);
        text_view_date = findViewById(R.id.txt_view_date);
        btn_save = findViewById(R.id.btn_save);
        et_name_title = findViewById(R.id.et_name);
        radioGroup = findViewById(R.id.radio_group);

        dialog_message = new Dialog(this);

        spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> spinnerView = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, play_role);
        spinnerView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerView);

        btn_date.setOnClickListener(v -> {
            if (v == btn_date) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, (view, year, month, dayOfMonth) -> text_view_date.setText(dayOfMonth + "-" + (month + 1) + "-" + year), mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btn_save.setOnClickListener(v -> {

            int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
            if (selectedRadioButtonId != -1) {
                selectedRadioButton = findViewById(selectedRadioButtonId);
                selectedRbText = selectedRadioButton.getText().toString();

            } else {
                selectedRbText = "Nothing selected";
            }

            new AlertDialog.Builder(ProfileActivity.this)
                    .setIcon(android.R.drawable.ic_menu_info_details)
                    .setTitle("INFORMATION")
                    .setMessage("Name : " + et_name_title.getEditableText().toString() + "\n\nBirthday : " + text_view_date.getText().toString() + "\n\nGender : " + selectedRbText + "\n\nSkill : " + spinner.getSelectedItem().toString())
                    .setNegativeButton("Cancel", null)
                    .show();
        });
    }
}