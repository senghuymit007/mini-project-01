package com.senghuy.miniproject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

public class NoteActivity extends AppCompatActivity {

    Intent intent;
    FrameLayout fl_addItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        fl_addItem = findViewById(R.id.add_item);

        fl_addItem.setOnClickListener(view -> {

            intent = new Intent(NoteActivity.this, NoteAddNewActivity.class);
            startActivity(intent);
        });


    }
}